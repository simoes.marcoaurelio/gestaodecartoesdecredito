package br.com.gestaodecartoesdecredito.pagamento.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RespostaInserirPagamento {

    @JsonProperty("id")
    private int idPagamento;

    @JsonProperty("cartao_id")
    private int idCartao;

    @JsonProperty("descricao")
    private String descricaoPagamento;

    @JsonProperty("valor")
    private double valorPagamento;

    public RespostaInserirPagamento() {
    }

    public int getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        this.idPagamento = idPagamento;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricaoPagamento() {
        return descricaoPagamento;
    }

    public void setDescricaoPagamento(String descricaoPagamento) {
        this.descricaoPagamento = descricaoPagamento;
    }

    public double getValorPagamento() {
        return valorPagamento;
    }

    public void setValorPagamento(double valorPagamento) {
        this.valorPagamento = valorPagamento;
    }
}
