package br.com.gestaodecartoesdecredito.pagamento.dtos;

import br.com.gestaodecartoesdecredito.cartao.models.Cartao;
import br.com.gestaodecartoesdecredito.pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento paraPagamento(RequisicaoInserirPagamento requisicaoInserirPagamento) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricaoPagamento(requisicaoInserirPagamento.getDescricaoPagamento());
        pagamento.setValorPagamento(requisicaoInserirPagamento.getValorPagamento());

        Cartao cartao = new Cartao();
        cartao.setIdCartao(requisicaoInserirPagamento.getIdCartao());

        pagamento.setCartao(cartao);

        return pagamento;
    }

    public RespostaInserirPagamento paraRespostaInserirPagamento(Pagamento pagamento) {
        RespostaInserirPagamento respostaInserirPagamento = new RespostaInserirPagamento();

        respostaInserirPagamento.setIdPagamento(pagamento.getIdPagamento());
        respostaInserirPagamento.setIdCartao(pagamento.getCartao().getIdCartao());
        respostaInserirPagamento.setDescricaoPagamento(pagamento.getDescricaoPagamento());
        respostaInserirPagamento.setValorPagamento(pagamento.getValorPagamento());

        return respostaInserirPagamento;
    }
}
