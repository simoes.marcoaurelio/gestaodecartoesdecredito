package br.com.gestaodecartoesdecredito.pagamento.repositories;

import br.com.gestaodecartoesdecredito.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByCartao_idCartao(int idCartao);
}