package br.com.gestaodecartoesdecredito.pagamento.models;

import br.com.gestaodecartoesdecredito.cartao.models.Cartao;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPagamento;

    @Size(min = 2, max = 100, message = "A descrição do pagamento deve ter entre 2 e 100 caracteres")
    private String descricaoPagamento;

    @DecimalMin(value = "0.1", message = "Insira um valor maior que zero para o pagamento")
    private double valorPagamento;

    @ManyToOne
    @NotNull(message = "O pagamento precisa estar atrelado a um cartão")
    private Cartao cartao;

    public Pagamento() {
    }

    public int getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        this.idPagamento = idPagamento;
    }

    public String getDescricaoPagamento() {
        return descricaoPagamento;
    }

    public void setDescricaoPagamento(String descricaoPagamento) {
        this.descricaoPagamento = descricaoPagamento;
    }

    public double getValorPagamento() {
        return valorPagamento;
    }

    public void setValorPagamento(double valorPagamento) {
        this.valorPagamento = valorPagamento;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }
}
