package br.com.gestaodecartoesdecredito.pagamento.controllers;

import br.com.gestaodecartoesdecredito.pagamento.dtos.PagamentoMapper;
import br.com.gestaodecartoesdecredito.pagamento.dtos.RequisicaoInserirPagamento;
import br.com.gestaodecartoesdecredito.pagamento.dtos.RespostaInserirPagamento;
import br.com.gestaodecartoesdecredito.pagamento.models.Pagamento;
import br.com.gestaodecartoesdecredito.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaInserirPagamento inserirPagamento(
            @RequestBody @Valid RequisicaoInserirPagamento requisicaoInserirPagamento) {
        try {
            Pagamento pagamento = pagamentoMapper.paraPagamento(requisicaoInserirPagamento);

            pagamento = pagamentoService.inserirPagamento(pagamento);

            return pagamentoMapper.paraRespostaInserirPagamento(pagamento);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<RespostaInserirPagamento> consultarPagamentosDeUmCartao(@PathVariable int id_cartao) {
        try {
            List<Pagamento> pagamentos = pagamentoService.consultarPagamentosDeUmCartao(id_cartao);

            List<RespostaInserirPagamento> listaDePagamentos = new ArrayList<>();

            for (Pagamento pagamento : pagamentos) {
                RespostaInserirPagamento respostaInserirPagamento =
                        pagamentoMapper.paraRespostaInserirPagamento(pagamento);
                listaDePagamentos.add(respostaInserirPagamento);
            }
            return listaDePagamentos;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
