package br.com.gestaodecartoesdecredito.pagamento.services;

import br.com.gestaodecartoesdecredito.cartao.models.Cartao;
import br.com.gestaodecartoesdecredito.cartao.repositories.CartaoRepository;
import br.com.gestaodecartoesdecredito.pagamento.models.Pagamento;
import br.com.gestaodecartoesdecredito.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public Pagamento inserirPagamento(Pagamento pagamento) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(pagamento.getCartao().getIdCartao());

        if (optionalCartao.isPresent()) {
            Cartao cartao = optionalCartao.get();
            pagamento.setCartao(cartao);

            return pagamentoRepository.save(pagamento);
        }
        else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public List<Pagamento> consultarPagamentosDeUmCartao(int idCartao) {
        List<Pagamento> listaDePagamentos = pagamentoRepository.findAllByCartao_idCartao(idCartao);

        if (listaDePagamentos.size() > 0) {
            return listaDePagamentos;
        }
        else {
            throw new RuntimeException("Não existem pagamentos para o cartão solicitado");
        }
    }
}
