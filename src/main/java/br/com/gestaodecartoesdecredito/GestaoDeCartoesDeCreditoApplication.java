package br.com.gestaodecartoesdecredito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoDeCartoesDeCreditoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoDeCartoesDeCreditoApplication.class, args);
	}

}
