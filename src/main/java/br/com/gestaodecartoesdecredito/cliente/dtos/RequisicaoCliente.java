package br.com.gestaodecartoesdecredito.cliente.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequisicaoCliente {

    @JsonProperty("name")
    private String nomeCliente;

    public RequisicaoCliente() {
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
}
