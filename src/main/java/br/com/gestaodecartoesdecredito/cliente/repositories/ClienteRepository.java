package br.com.gestaodecartoesdecredito.cliente.repositories;

import br.com.gestaodecartoesdecredito.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
