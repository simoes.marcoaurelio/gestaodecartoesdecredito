package br.com.gestaodecartoesdecredito.cliente.controllers;

import br.com.gestaodecartoesdecredito.cliente.dtos.ClienteMapper;
import br.com.gestaodecartoesdecredito.cliente.dtos.RequisicaoCliente;
import br.com.gestaodecartoesdecredito.cliente.dtos.RespostaCliente;
import br.com.gestaodecartoesdecredito.cliente.models.Cliente;
import br.com.gestaodecartoesdecredito.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCliente cadastrarCliente(
            @RequestBody @Valid RequisicaoCliente requisicaoCliente) {
        Cliente cliente = clienteMapper.paraCliente(requisicaoCliente);

        cliente = clienteService.cadastrarCliente(cliente);

        return clienteMapper.paraRespostaCadastrarCliente(cliente);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaCliente consultarClientePorId(@PathVariable int id) {
        try {
            Cliente cliente = clienteService.consultarClientePorId(id);
            return clienteMapper.paraRespostaCadastrarCliente(cliente);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
