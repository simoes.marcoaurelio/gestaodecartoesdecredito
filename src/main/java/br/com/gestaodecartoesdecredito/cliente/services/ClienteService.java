package br.com.gestaodecartoesdecredito.cliente.services;

import br.com.gestaodecartoesdecredito.cliente.models.Cliente;
import br.com.gestaodecartoesdecredito.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int idCliente) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(idCliente);

        if (optionalCliente.isPresent()) {
            return optionalCliente.get();
        }
        else {
            throw new RuntimeException("Cliente não encontrado");
        }
    }
}
