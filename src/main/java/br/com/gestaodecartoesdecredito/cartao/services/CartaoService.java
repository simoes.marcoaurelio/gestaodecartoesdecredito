package br.com.gestaodecartoesdecredito.cartao.services;

import br.com.gestaodecartoesdecredito.cartao.dtos.RequisicaoAtualizarCartao;
import br.com.gestaodecartoesdecredito.cartao.models.Cartao;
import br.com.gestaodecartoesdecredito.cartao.repositories.CartaoRepository;
import br.com.gestaodecartoesdecredito.cliente.models.Cliente;
import br.com.gestaodecartoesdecredito.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public Cartao cadastrarCartao(Cartao cartao) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(cartao.getCliente().getIdCliente());

        if (optionalCliente.isPresent()) {
            cartao.setCliente(optionalCliente.get());
            cartao.setCartaoAtivo(false);
            return cartaoRepository.save(cartao);
        }
        else {
            throw new RuntimeException("Cliente não encontrado");
        }
    }

    public Cartao atualizarStatusCartao(String numeroCartao, RequisicaoAtualizarCartao requisicaoAtualizarCartao) {
        Cartao cartao = consultarCartaoPeloNumero(numeroCartao);
        cartao.setCartaoAtivo(requisicaoAtualizarCartao.isCartaoAtivo());

        return cartaoRepository.save(cartao);
    }

    public Cartao consultarCartaoPeloNumero(String numeroCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumeroCartao(numeroCartao);

        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }
}
