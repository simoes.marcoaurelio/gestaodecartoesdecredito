package br.com.gestaodecartoesdecredito.cartao.controllers;

import br.com.gestaodecartoesdecredito.cartao.dtos.*;
import br.com.gestaodecartoesdecredito.cartao.models.Cartao;
import br.com.gestaodecartoesdecredito.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCadastrarOuAtualizarCartao cadastrarCartao(
            @RequestBody @Valid RequisicaoCadastrarCartao requisicaoCadastrarCartao) {
        try {
            Cartao cartao = cartaoMapper.paraCartao(requisicaoCadastrarCartao);

            cartao = cartaoService.cadastrarCartao(cartao);

            return cartaoMapper.paraRespostaCadastrarOuAtualizarCartao(cartao);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaCadastrarOuAtualizarCartao atualizarStatusCartao(
            @PathVariable String numero, @RequestBody @Valid RequisicaoAtualizarCartao requisicaoAtualizarCartao) {
        try {
            Cartao cartao = cartaoService.atualizarStatusCartao(numero,requisicaoAtualizarCartao);

            return cartaoMapper.paraRespostaCadastrarOuAtualizarCartao(cartao);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public RespostaConsultarCartao consultarCartaoPeloNumero(@PathVariable String numero) {
        try {
            Cartao cartao = cartaoService.consultarCartaoPeloNumero(numero);
            return cartaoMapper.paraRespostaConsultarCartao(cartao);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
