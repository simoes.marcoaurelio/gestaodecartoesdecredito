package br.com.gestaodecartoesdecredito.cartao.dtos;

import br.com.gestaodecartoesdecredito.cartao.models.Cartao;
import br.com.gestaodecartoesdecredito.cliente.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao paraCartao(RequisicaoCadastrarCartao requisicaoCadastrarCartao) {
        Cartao cartao = new Cartao();
        cartao.setNumeroCartao(requisicaoCadastrarCartao.getNumeroCartao());

        Cliente cliente = new Cliente();
        cliente.setIdCliente(requisicaoCadastrarCartao.getIdCliente());
        cartao.setCliente(cliente);

        return cartao;
    }

    public RespostaCadastrarOuAtualizarCartao paraRespostaCadastrarOuAtualizarCartao(Cartao cartao) {
        RespostaCadastrarOuAtualizarCartao respostaCadastrarOuAtualizarCartao =
                new RespostaCadastrarOuAtualizarCartao();

        respostaCadastrarOuAtualizarCartao.setIdCartao(cartao.getIdCartao());
        respostaCadastrarOuAtualizarCartao.setNumeroCartao(cartao.getNumeroCartao());
        respostaCadastrarOuAtualizarCartao.setIdCliente(cartao.getCliente().getIdCliente());
        respostaCadastrarOuAtualizarCartao.setCartaoAtivo(cartao.isCartaoAtivo());

        return respostaCadastrarOuAtualizarCartao;
    }

    public RespostaConsultarCartao paraRespostaConsultarCartao(Cartao cartao) {
        RespostaConsultarCartao respostaConsultarCartao = new RespostaConsultarCartao();

        respostaConsultarCartao.setIdCartao(cartao.getIdCartao());
        respostaConsultarCartao.setNumeroCartao(cartao.getNumeroCartao());
        respostaConsultarCartao.setIdCliente(cartao.getCliente().getIdCliente());

        return respostaConsultarCartao;
    }
}