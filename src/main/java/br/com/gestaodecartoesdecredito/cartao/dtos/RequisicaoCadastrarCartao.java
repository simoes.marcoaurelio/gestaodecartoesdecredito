package br.com.gestaodecartoesdecredito.cartao.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequisicaoCadastrarCartao {

    @JsonProperty("numero")
    private String numeroCartao;

    @JsonProperty("clienteId")
    private int idCliente;

    public RequisicaoCadastrarCartao() {
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}
